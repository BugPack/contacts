package com.example.domain.model

import java.io.Serializable

data class EducationPeriodModel(
    var start: String,
    var end: String
) : Serializable