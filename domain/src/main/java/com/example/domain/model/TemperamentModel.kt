package com.example.domain.model

import java.io.Serializable

enum class TemperamentModel(var character: String) : Serializable {
    melancholic("melancholic"),
    phlegmatic("phlegmatic"),
    sanguine("sanguine"),
    choleric("choleric")
}