package com.example.domain.model

import java.io.Serializable

data class ContactModel(
    var id: String,
    var name: String,
    var phone: String,
    var height: String,
    var biography: String,
    var temperament: TemperamentModel,
    var educationPeriod: EducationPeriodModel
) : Serializable