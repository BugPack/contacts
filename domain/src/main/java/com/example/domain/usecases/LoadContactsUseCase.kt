package com.example.domain.usecases

import com.example.domain.model.ContactModel
import io.reactivex.Flowable

interface LoadContactsUseCase {
    fun loadContacts(): Flowable<List<ContactModel>>
}