package com.example.domain.usecases

import com.example.domain.model.ContactModel
import com.example.domain.repository.ContactsDBRepo

class SaveContactsUseCaseImpl(private val contactsDBRepo: ContactsDBRepo) : SaveContactsUseCase {
    override fun saveContacts(contacts: List<ContactModel>) {
        contactsDBRepo.insertAll(contacts)
    }
}