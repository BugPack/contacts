package com.example.domain.usecases

import com.example.domain.model.ContactModel

interface SaveContactsUseCase {
    fun saveContacts(contacts: List<ContactModel>)
}