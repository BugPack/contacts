package com.example.domain.usecases

import com.example.domain.model.ContactModel
import io.reactivex.Flowable

interface GetContactsUseCase {
    fun getContacts(listNumber: Int): Flowable<List<ContactModel>>
}