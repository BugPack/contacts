package com.example.domain.usecases

import com.example.domain.model.ContactModel
import com.example.domain.repository.ContactsRepo
import io.reactivex.Flowable

class GetContactsUseCaseImpl(private val contactsRepo: ContactsRepo) : GetContactsUseCase {
    override fun getContacts(listNumber: Int): Flowable<List<ContactModel>> {
        return contactsRepo.getContactsFlow(listNumber)
    }
}