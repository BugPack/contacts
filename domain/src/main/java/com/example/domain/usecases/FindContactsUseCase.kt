package com.example.domain.usecases

import com.example.domain.model.ContactModel
import io.reactivex.Flowable

interface FindContactsUseCase {
    fun findContacts(query: String): Flowable<List<ContactModel>>
}