package com.example.domain.usecases

import com.example.domain.model.ContactModel
import com.example.domain.repository.ContactsDBRepo
import io.reactivex.Flowable

class LoadContactsUseCaseImpl(private val contactsDBRepo: ContactsDBRepo) : LoadContactsUseCase {
    override fun loadContacts(): Flowable<List<ContactModel>> {
        return contactsDBRepo.getAll()
    }
}