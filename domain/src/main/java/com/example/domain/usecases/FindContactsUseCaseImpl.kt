package com.example.domain.usecases

import com.example.domain.model.ContactModel
import com.example.domain.repository.ContactsDBRepo
import io.reactivex.Flowable

class FindContactsUseCaseImpl(private val contactsDBRepo: ContactsDBRepo) : FindContactsUseCase {
    override fun findContacts(query: String): Flowable<List<ContactModel>> {
        return contactsDBRepo.findContacts(query)
    }
}