package com.example.domain.di

import com.example.domain.usecases.*
import org.koin.dsl.module

val domainModule = module {
    factory<GetContactsUseCase> { GetContactsUseCaseImpl(contactsRepo = get()) }
    factory<SaveContactsUseCase> { SaveContactsUseCaseImpl(contactsDBRepo = get()) }
    factory<LoadContactsUseCase> { LoadContactsUseCaseImpl(contactsDBRepo = get()) }
    factory<FindContactsUseCase> { FindContactsUseCaseImpl(contactsDBRepo = get()) }
}