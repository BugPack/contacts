package com.example.domain.repository

import com.example.domain.model.ContactModel
import io.reactivex.Flowable

interface ContactsDBRepo {
    fun getAll(): Flowable<List<ContactModel>>
    fun findContacts(query: String): Flowable<List<ContactModel>>
    fun insertAll(contacts: List<ContactModel>)
    fun update(contact: ContactModel)
    fun delete(contact: ContactModel)
}