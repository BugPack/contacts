package com.example.domain.repository

import com.example.domain.model.ContactModel
import io.reactivex.Flowable

interface ContactsRepo {
    fun getContactsFlow(list: Int): Flowable<List<ContactModel>>
}