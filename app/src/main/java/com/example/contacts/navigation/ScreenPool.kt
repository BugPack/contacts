package com.example.contacts.navigation

import com.example.contacts.ui.fragment.ContactFragment
import com.example.contacts.ui.fragment.ContactsFragment
import com.example.domain.model.ContactModel
import ru.terrakok.cicerone.android.support.SupportAppScreen

object ScreenPool {
    class ContactsScreen : SupportAppScreen() {
        override fun getFragment() = ContactsFragment.getInstance()
    }

    class ContactScreen(private val contact: ContactModel) : SupportAppScreen() {
        override fun getFragment() = ContactFragment.getInstance(contact)
    }
}