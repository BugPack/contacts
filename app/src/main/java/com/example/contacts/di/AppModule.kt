package com.example.contacts.di

import com.example.contacts.viewmodel.ContactViewModel
import com.example.contacts.viewmodel.ContactsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

val appModule = module {
    single { Cicerone.create() }
    single<Router> { get<Cicerone<Router>>().router }
    single<NavigatorHolder> { get<Cicerone<Router>>().navigatorHolder }

    viewModel {
        ContactsViewModel(
            getContactsUseCase = get(),
            router = get(),
            saveContactsUseCase = get(),
            loadContactsUseCase = get(),
            findContactsUseCase = get()
        )
    }
    viewModel { ContactViewModel(router = get()) }
}