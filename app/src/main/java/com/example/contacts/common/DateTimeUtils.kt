package com.example.contacts.common

import java.text.SimpleDateFormat
import java.util.*

object DateTimeUtils {
    const val SERVER_DATE_TIME = "yyyy-MM-dd'T'HH:mm:ss"
    const val DATE_TIME = "dd.MM.yyyy HH:mm"
    const val DATE = "dd.MM.yyyy"

    fun convert(dateTime: String, formatIn: String, formatOut: String): String {
        return try {
            val sdfIn = SimpleDateFormat(formatIn, Locale.getDefault())
            val sdfOut = SimpleDateFormat(formatOut, Locale.getDefault())
            val date = sdfIn.parse(dateTime)
            sdfOut.format(date)
        } catch (e: Exception) {
            ""
        }
    }
}