package com.example.contacts.common

import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

@BindingAdapter("isVisible")
fun View.isVisible(visible: Boolean) {
    this.isVisible = visible
}

@BindingAdapter("textCap")
fun TextView.textCap(text: String) {
    this.text = String.format(text).capitalize()
}

@BindingAdapter("onRefresh")
fun SwipeRefreshLayout.onRefresh(onRefresh: () -> Unit) {
    this.setOnRefreshListener {
        this.isRefreshing = false
        onRefresh()
    }
}

@BindingAdapter("onClickNav")
fun Toolbar.onClickNav(onClick: () -> Unit) {
    this.setNavigationOnClickListener {
        onClick()
    }
}