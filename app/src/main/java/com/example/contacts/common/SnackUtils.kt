package com.example.contacts.common

import android.view.View
import com.google.android.material.snackbar.Snackbar

object SnackUtils {
    private var snack: Snackbar? = null

    fun show(view: View, message: String) {
        snack?.dismiss()
        snack = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE)
        snack?.show()
    }

    fun hide() {
        snack?.dismiss()
    }
}