package com.example.contacts.common

import com.example.contacts.App
import com.example.contacts.R
import retrofit2.HttpException
import java.net.UnknownHostException

object ErrorUtils {

    fun proceed(
        throwable: Throwable,
        messageListener: (message: String) -> Unit = {}
    ) {
        when (throwable) {
            is HttpException -> {
                messageListener(App.INSTANCE.getString(R.string.error_general))
            }
            is UnknownHostException -> {
                messageListener(App.INSTANCE.getString(R.string.error_network_connection))
            }
            else -> {
                messageListener(App.INSTANCE.getString(R.string.error_general))
            }
        }
    }
}