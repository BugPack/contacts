package com.example.contacts.common

import androidx.preference.PreferenceManager
import com.example.contacts.App

object Settings {
    private val PREFERENCES = PreferenceManager.getDefaultSharedPreferences(App.INSTANCE)

    private const val LAST_LOAD_CONTACTS_KEY = "last_load_contacts_key"

    var lastLoadContactsTime: Long
        get() = PREFERENCES.getLong(LAST_LOAD_CONTACTS_KEY, 0L)
        set(value) {
            PREFERENCES.edit().putLong(LAST_LOAD_CONTACTS_KEY, value).apply()
        }
}