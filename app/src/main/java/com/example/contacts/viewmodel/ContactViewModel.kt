package com.example.contacts.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.contacts.common.DateTimeUtils
import com.example.domain.model.ContactModel
import com.example.domain.model.EducationPeriodModel
import ru.terrakok.cicerone.Router

class ContactViewModel(private val router: Router) : ViewModel() {
    var contact = ObservableField<ContactModel>()
    var callPhone = MutableLiveData<String>()

    fun getEducationPeriod(educationPeriodModel: EducationPeriodModel): String {
        val start = DateTimeUtils.convert(
            educationPeriodModel.start,
            DateTimeUtils.SERVER_DATE_TIME,
            DateTimeUtils.DATE
        )
        val end = DateTimeUtils.convert(
            educationPeriodModel.end,
            DateTimeUtils.SERVER_DATE_TIME,
            DateTimeUtils.DATE
        )
        return String.format("$start - $end")
    }

    fun callPhone(phone: String) {
        callPhone.postValue(phone)
    }

    fun clickBack() {
        router.exit()
    }
}