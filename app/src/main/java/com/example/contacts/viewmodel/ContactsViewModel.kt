package com.example.contacts.viewmodel

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.contacts.common.ErrorUtils
import com.example.contacts.common.Settings
import com.example.contacts.navigation.ScreenPool
import com.example.contacts.ui.adapter.ContactsAdapter
import com.example.domain.usecases.FindContactsUseCase
import com.example.domain.usecases.GetContactsUseCase
import com.example.domain.usecases.LoadContactsUseCase
import com.example.domain.usecases.SaveContactsUseCase
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import ru.terrakok.cicerone.Router

class ContactsViewModel(
    private val getContactsUseCase: GetContactsUseCase,
    private val router: Router,
    private val saveContactsUseCase: SaveContactsUseCase,
    private val loadContactsUseCase: LoadContactsUseCase,
    private val findContactsUseCase: FindContactsUseCase
) : ViewModel() {
    private val adapter = ContactsAdapter { router.navigateTo(ScreenPool.ContactScreen(it)) }
    private val disposables = CompositeDisposable()
    var loading = ObservableBoolean(false)
    var snackMutable = MutableLiveData<String>()

    var clearBtnVisible = ObservableBoolean(false)
    var search = ObservableField<String>()

    override fun onCleared() {
        disposables.dispose()
        super.onCleared()
    }

    fun getAdapter() = adapter

    fun initLoadingContacts() {
        if (Settings.lastLoadContactsTime == 0L ||
            System.currentTimeMillis() > Settings.lastLoadContactsTime + 60000L
        ) {
            getContacts()
        } else {
            loadContacts()
        }
    }

    private fun getContacts() {
        loading.set(true)
        disposables.add(
            Flowable.concat(
                getContactsUseCase.getContacts(1),
                getContactsUseCase.getContacts(2),
                getContactsUseCase.getContacts(3)
            ).doFinally {
                loading.set(false)
            }.subscribe({
                hideSnack()
                Settings.lastLoadContactsTime = System.currentTimeMillis()
                saveContactsUseCase.saveContacts(it)
                loadContacts()
            }, { e ->
                loadContacts()
                ErrorUtils.proceed(e) {
                    showSnack(it)
                }
            })
        )
    }

    private fun loadContacts() {
        disposables.add(
            loadContactsUseCase.loadContacts().subscribe({
                adapter.addItems(it)
            }, {})
        )
    }

    fun refresh() {
        adapter.clear()
        getContacts()
    }

    private fun showSnack(message: String) {
        snackMutable.postValue(message)
    }

    private fun hideSnack() {
        snackMutable.postValue("")
    }

    fun searchTextChanged(input: String) {
        if (input.isEmpty()) {
            loadContacts()
        } else {
            findContacts(input)
        }
        clearBtnVisible.set(input.isNotEmpty())
    }

    private fun findContacts(query: String) {
        disposables.add(
            findContactsUseCase.findContacts(query).subscribe({
                adapter.addItems(it)
            }, {})
        )
    }

    fun clearSearch() {
        search.set("")
    }
}