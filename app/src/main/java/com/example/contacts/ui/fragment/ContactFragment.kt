package com.example.contacts.ui.fragment

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.contacts.R
import com.example.contacts.common.Constants
import com.example.contacts.databinding.FragmentContactBinding
import com.example.contacts.ui.activity.MainActivity
import com.example.contacts.viewmodel.ContactViewModel
import com.example.domain.model.ContactModel
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_contact.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ContactFragment : Fragment() {
    companion object {
        fun getInstance(contact: ContactModel) = ContactFragment().apply {
            arguments = Bundle().apply {
                putSerializable(Constants.CONTACT_KEY, contact)
            }
        }
    }

    private val viewModel: ContactViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            if (it.containsKey(Constants.CONTACT_KEY)) {
                viewModel.contact.set(it.getSerializable(Constants.CONTACT_KEY) as ContactModel)
            }
        }
        (activity as? MainActivity)?.setSupportActionBar(toolbar)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return DataBindingUtil.inflate<FragmentContactBinding>(
            inflater,
            R.layout.fragment_contact,
            container,
            false
        ).apply { viewModel = this@ContactFragment.viewModel }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.callPhone.observe(viewLifecycleOwner, Observer {
            RxPermissions(this.requireActivity())
                .request(Manifest.permission.CALL_PHONE)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { isGranted ->
                    if (isGranted) {
                        startActivity(Intent(Intent.ACTION_CALL, Uri.parse("tel:$it")))
                    }
                }
        })
    }
}