package com.example.contacts.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.contacts.R
import com.example.domain.model.ContactModel
import kotlinx.android.synthetic.main.list_item_contact.view.*

class ContactsAdapter(var onClickItem: (ContactModel) -> Unit) :
    RecyclerView.Adapter<ContactsAdapter.ContactsHolder>() {
    private val dataset = arrayListOf<ContactModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ContactsHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.list_item_contact, parent, false)
    )

    override fun getItemCount() = dataset.size

    override fun onBindViewHolder(holder: ContactsAdapter.ContactsHolder, position: Int) {
        val item = dataset[position]
        holder.apply {
            name?.text = item.name
            height?.text = item.height
            phone?.text = item.phone
            rootLayout?.setOnClickListener {
                onClickItem(item)
            }
        }
    }

    fun addItems(items: List<ContactModel>) {
        dataset.clear()
        dataset.addAll(items)
        notifyDataSetChanged()
    }

    fun clear() {
        dataset.clear()
        notifyDataSetChanged()
    }

    class ContactsHolder(view: View) : RecyclerView.ViewHolder(view) {
        val rootLayout = view.rootLayout
        val name = view.tvName
        val height = view.tvHeight
        val phone = view.tvPhone
    }
}