package com.example.contacts.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.Observable
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.contacts.R
import com.example.contacts.common.SnackUtils
import com.example.contacts.databinding.FragmentContactsBinding
import com.example.contacts.ui.activity.MainActivity
import com.example.contacts.viewmodel.ContactsViewModel
import kotlinx.android.synthetic.main.fragment_contacts.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ContactsFragment : Fragment() {
    companion object {
        fun getInstance() = ContactsFragment()
    }

    private val viewModel: ContactsViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return DataBindingUtil.inflate<FragmentContactsBinding>(
            inflater,
            R.layout.fragment_contacts,
            container,
            false
        ).apply { viewModel = this@ContactsFragment.viewModel }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        initSnackBar()
        initSearch()
        viewModel.initLoadingContacts()
    }

    private fun initAdapter() {
        rvContacts?.layoutManager = LinearLayoutManager(context)
        rvContacts?.adapter = viewModel.getAdapter()
    }

    private fun initSnackBar() {
        viewModel.snackMutable.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it.isEmpty()) {
                    SnackUtils.hide()
                } else {
                    SnackUtils.show(rootLayout, it)
                }
            }
        })
    }

    private fun initSearch() {
        viewModel.search.addOnPropertyChangedCallback(object :
            Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                viewModel.search.get()?.let {
                    viewModel.searchTextChanged(it)
                }
            }
        })
    }
}