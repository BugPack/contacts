package com.example.data.apiservice

import com.example.data.model.DataContactModel
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {
    @GET("SkbkonturMobile/mobile-test-droid/master/json/generated-0{listNumber}.json")
    fun getContacts(@Path("listNumber") listNumber: Int): Flowable<List<DataContactModel>>
}