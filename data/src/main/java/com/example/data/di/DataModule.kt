package com.example.data.di

import androidx.room.Room
import com.example.data.db.ContactsDatabase
import com.example.data.repository.ContactsDBRepoImpl
import com.example.data.repository.ContactsRepoImpl
import com.example.domain.repository.ContactsDBRepo
import com.example.domain.repository.ContactsRepo
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val dataModule = module {
    single {
        Room.databaseBuilder(androidContext(), ContactsDatabase::class.java, "contactsDatabase")
            .fallbackToDestructiveMigration()
            .build()
    }
    single { get<ContactsDatabase>().contactsDao() }

    factory<ContactsRepo> { ContactsRepoImpl(apiService = get()) }
    factory<ContactsDBRepo> { ContactsDBRepoImpl(contactsDao = get()) }
}