package com.example.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DataContactModel(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("phone") val phone: String,
    @SerializedName("height") val height: String,
    @SerializedName("biography") val biography: String,
    @SerializedName("temperament") val temperament: DataTemperamentModel,
    @SerializedName("educationPeriod") val educationPeriod: DataEducationPeriodModel
) : Serializable