package com.example.data.model

import java.io.Serializable

enum class DataTemperamentModel(val character: String) : Serializable {
    melancholic("melancholic"),
    phlegmatic("phlegmatic"),
    sanguine("sanguine"),
    choleric("choleric")
}