package com.example.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DataEducationPeriodModel(
    @SerializedName("start") val start: String,
    @SerializedName("end") val end: String
) : Serializable