package com.example.data.db

import androidx.room.TypeConverter
import com.example.data.db.model.TemperamentEntity

class TemperamentConverter {
    @TypeConverter
    fun fromTemperament(value: TemperamentEntity): Int {
        return value.ordinal
    }

    @TypeConverter
    fun toTemperament(value: Int): TemperamentEntity {
        return when (value) {
            0 -> TemperamentEntity.melancholic
            1 -> TemperamentEntity.phlegmatic
            2 -> TemperamentEntity.sanguine
            else -> TemperamentEntity.choleric
        }
    }
}