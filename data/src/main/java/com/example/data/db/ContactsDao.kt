package com.example.data.db

import androidx.room.*
import com.example.data.db.model.ContactEntity
import io.reactivex.Flowable

@Dao
interface ContactsDao {
    @Query("SELECT * FROM contactentity ORDER BY name ASC")
    fun getAll(): Flowable<List<ContactEntity>>

    @Query(
        "SELECT * FROM contactentity " +
                "WHERE name LIKE '%'||:query||'%' " +
                "OR phone LIKE '%'||:query||'%' ORDER BY name ASC"
    )
    fun findContacts(query: String): Flowable<List<ContactEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(contacts: List<ContactEntity>)

    @Update
    fun update(contact: ContactEntity)

    @Delete
    fun delete(contact: ContactEntity)
}