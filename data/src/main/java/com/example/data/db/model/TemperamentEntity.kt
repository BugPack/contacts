package com.example.data.db.model

import java.io.Serializable

enum class TemperamentEntity(val character: String) : Serializable {
    melancholic("melancholic"),
    phlegmatic("phlegmatic"),
    sanguine("sanguine"),
    choleric("choleric")
}