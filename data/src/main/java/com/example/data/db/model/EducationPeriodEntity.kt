package com.example.data.db.model

data class EducationPeriodEntity(
    val start: String,
    val end: String
)