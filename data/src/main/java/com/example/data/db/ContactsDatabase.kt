package com.example.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.data.db.model.ContactEntity

@Database(entities = [ContactEntity::class], version = 1, exportSchema = false)
@TypeConverters(TemperamentConverter::class)
abstract class ContactsDatabase : RoomDatabase() {
    abstract fun contactsDao(): ContactsDao
}