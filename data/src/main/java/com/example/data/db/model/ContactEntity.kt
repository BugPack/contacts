package com.example.data.db.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.data.model.DataEducationPeriodModel
import com.example.data.model.DataTemperamentModel

@Entity
data class ContactEntity(
    @PrimaryKey
    val id: String,
    val name: String,
    val phone: String,
    val height: String,
    val biography: String,
    val temperament: TemperamentEntity,
    @Embedded
    val educationPeriod: EducationPeriodEntity
)