package com.example.data.repository

import com.example.data.db.ContactsDao
import com.example.data.db.model.ContactEntity
import com.example.data.db.model.EducationPeriodEntity
import com.example.data.db.model.TemperamentEntity
import com.example.domain.model.ContactModel
import com.example.domain.model.EducationPeriodModel
import com.example.domain.model.TemperamentModel
import com.example.domain.repository.ContactsDBRepo
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ContactsDBRepoImpl(private val contactsDao: ContactsDao) : ContactsDBRepo {

    override fun getAll(): Flowable<List<ContactModel>> {
        return contactsDao.getAll()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .flatMap { list ->
                Flowable.fromIterable(list)
                    .map { item -> convertContactEntityToContactModel(item) }
                    .toList()
                    .toFlowable()
            }
    }

    override fun findContacts(query: String): Flowable<List<ContactModel>> {
        return contactsDao.findContacts(query)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .flatMap { list ->
                Flowable.fromIterable(list)
                    .map { item -> convertContactEntityToContactModel(item) }
                    .toList()
                    .toFlowable()
            }
    }

    override fun insertAll(contacts: List<ContactModel>) {
        Flowable.fromIterable(contacts)
            .subscribeOn(Schedulers.io())
            .map { item -> convertContactModelToContactEntity(item) }
            .toList()
            .subscribe { list ->
                Observable.create { o: ObservableEmitter<Void> ->
                    contactsDao.insert(list)
                    o.onComplete()
                }.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe()
            }
    }

    override fun update(contact: ContactModel) {
        Observable.create { o: ObservableEmitter<Void> ->
            contactsDao.update(convertContactModelToContactEntity(contact))
            o.onComplete()
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    override fun delete(contact: ContactModel) {
        Observable.create { o: ObservableEmitter<Void> ->
            contactsDao.delete(convertContactModelToContactEntity(contact))
            o.onComplete()
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    private fun convertContactEntityToContactModel(contactEntity: ContactEntity): ContactModel {
        return ContactModel(
            id = contactEntity.id,
            name = contactEntity.name,
            phone = contactEntity.phone,
            height = contactEntity.height,
            biography = contactEntity.biography,
            temperament = TemperamentModel.valueOf(contactEntity.temperament.character),
            educationPeriod = EducationPeriodModel(
                start = contactEntity.educationPeriod.start,
                end = contactEntity.educationPeriod.end
            )
        )
    }

    private fun convertContactModelToContactEntity(contactModel: ContactModel): ContactEntity {
        return ContactEntity(
            id = contactModel.id,
            name = contactModel.name,
            phone = contactModel.phone,
            height = contactModel.height,
            biography = contactModel.biography,
            temperament = TemperamentEntity.valueOf(contactModel.temperament.character),
            educationPeriod = EducationPeriodEntity(
                start = contactModel.educationPeriod.start,
                end = contactModel.educationPeriod.end
            )
        )
    }
}