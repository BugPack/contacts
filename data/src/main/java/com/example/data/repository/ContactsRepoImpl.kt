package com.example.data.repository

import com.example.data.apiservice.ApiService
import com.example.domain.model.ContactModel
import com.example.domain.model.EducationPeriodModel
import com.example.domain.model.TemperamentModel
import com.example.domain.repository.ContactsRepo
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ContactsRepoImpl(private val apiService: ApiService) : ContactsRepo {
    override fun getContactsFlow(listNumber: Int): Flowable<List<ContactModel>> {
        return apiService.getContacts(listNumber)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .flatMap { list ->
                Flowable.fromIterable(list)
                    .map { item ->
                        ContactModel(
                            id = item.id,
                            name = item.name,
                            phone = item.phone,
                            height = item.height,
                            biography = item.biography,
                            temperament = TemperamentModel.valueOf(item.temperament.character),
                            educationPeriod = EducationPeriodModel(
                                start = item.educationPeriod.start,
                                end = item.educationPeriod.end
                            )
                        )
                    }
                    .toList()
                    .toFlowable()
            }
    }
}